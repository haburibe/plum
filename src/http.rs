use std::collections::HashMap;
use std::fmt;
use std::io::{BufReader, BufRead, Write};
use std::net::{TcpListener, TcpStream, Shutdown};

type Handler = Box<Fn(&Request) -> Response>;

pub struct Application {
    handlers: HashMap<(Method, String), Handler>,
}

impl Application {
    pub fn new() -> Self {
        Self { handlers: HashMap::new() }
    }

    pub fn listen(&mut self, host: &str, port: u16) {
        let listener = TcpListener::bind(format!("{}:{}", host, port)).unwrap();
        for stream in listener.incoming() {
            match stream {
                Ok(stream) => {
                    self.handle_client(stream);
                }
                Err(e) => println!("error: {}", e),
            }
        }
    }

    pub fn add_handler(&mut self, method: &str, path: &str, handler: Handler) {
        self.handlers.insert(
            (Method::from_str(method), path.to_string()),
            handler,
        );
    }

    pub fn get(&mut self, path: &str, handler: Handler) {
        self.add_handler(Method::GET.to_str(), path, handler);
    }

    pub fn post(&mut self, path: &str, handler: Handler) {
        self.add_handler(Method::POST.to_str(), path, handler);
    }

    pub fn put(&mut self, path: &str, handler: Handler) {
        self.add_handler(Method::PUT.to_str(), path, handler);
    }

    pub fn delete(&mut self, path: &str, handler: Handler) {
        self.add_handler(Method::DELETE.to_str(), path, handler);
    }

    pub fn dispatch_request(&mut self, method: Method, path: &str, req: Request) -> Response {
        match self.handlers.get(&(method, path.to_string())) {
            Some(handler) => handler(&req),
            None => error_404_handler(&req),
        }
    }

    pub fn handle_client(&mut self, stream: TcpStream) {
        let mut reader = BufReader::new(stream);

        // parse request line
        let mut request_line = String::new();
        reader.read_line(&mut request_line).unwrap();
        let (method, target, version) = parse_request_line(&request_line);

        println!("< {} {} {}", method, target, version);

        // parse header lines
        let mut headers = Headers::new();
        loop {
            let mut line = String::new();
            reader.read_line(&mut line).unwrap();
            if line == "\r\n" {
                break;
            }

            let (name, value) = parse_header_line(&line);
            println!("< {}: {}", name, value);
            headers.insert(name, value);
        }

        let request = Request::new(method, target, version, headers);
        let resp: Response = self.dispatch_request(Method::from_str(method), target, request);

        let mut stream = reader.into_inner();
        stream.write(resp.render().as_bytes()).unwrap();

        // close connection
        stream.shutdown(Shutdown::Both).unwrap()
    }
}

#[derive(PartialEq, Eq, Hash)]
pub enum Method {
    GET,
    POST,
    PUT,
    DELETE,
    OPTIONS,
    HEAD,
}

impl Method {
    pub fn from_str(s: &str) -> Method {
        match s {
            "GET" => Method::GET,
            "POST" => Method::POST,
            "PUT" => Method::PUT,
            "DELETE" => Method::DELETE,
            "OPTIONS" => Method::OPTIONS,
            "HEAD" => Method::HEAD,
            _ => Method::GET,  // TODO: 未実装、または不正なメソッドの場合の処理
        }
    }

    fn to_str(&self) -> &str {
        match self {
            &Method::GET => "GET",
            &Method::POST => "POST",
            &Method::PUT => "PUT",
            &Method::DELETE => "DELETE",
            &Method::OPTIONS => "OPTIONS",
            &Method::HEAD => "HEAD",
        }
    }
}

pub enum Status {
    Ok,
    NotFound,
    InternalServerError,
}

impl Status {
    fn from_u16(&self, n: u16) -> Status {
        match n {
            200 => Status::Ok,
            404 => Status::NotFound,
            500 => Status::InternalServerError,
            _ => Status::InternalServerError,
        }
    }

    fn code(&self) -> u16 {
        match *self {
            Status::Ok => 200,
            Status::NotFound => 404,
            Status::InternalServerError => 500,
        }
    }

    fn reason(&self) -> &str {
        match *self {
            Status::Ok => "OK",
            Status::NotFound => "Not Found",
            Status::InternalServerError => "Internal Server Error",
        }
    }
}

impl fmt::Display for Status {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.code(), self.reason())
    }
}

pub type Headers = HashMap<String, String>;

pub struct Response {
    status: Status,
    headers: Headers,
    body: String,
}

impl Response {
    pub fn new(status: Status, headers: Headers, body: &str) -> Self {
        Self {
            status: status,
            headers: headers,
            body: body.to_string(),
        }
    }

    pub fn render(&self) -> String {
        format!("{}", self)
    }
}

impl fmt::Display for Response {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut result_str = String::new();

        // status line
        result_str += &format!("HTTP/1.1 {}\r\n", self.status);

        // header lines
        for (name, value) in &self.headers {
            result_str += &format!("{}: {}\r\n", name, value);
        }

        // blank line
        result_str += &format!("\r\n");
        // body
        result_str += &format!("{}", self.body);

        write!(f, "{}", result_str)
    }
}

pub struct Request {
    pub method: Method,
    pub target: String,
    pub version: String,
    pub headers: Headers,
}

impl Request {
    pub fn new(method: &str, target: &str, version: &str, headers: Headers) -> Self {
        Self {
            method: Method::from_str(method),
            target: target.to_string(),
            version: version.to_string(),
            headers: headers,
        }
    }
}

fn parse_request_line(line: &str) -> (&str, &str, &str) {
    let v: Vec<&str> = line.splitn(3, ' ').collect();
    (
        v[0].trim(), // method
        v[1].trim(), // target
        v[2].trim(), // http_version
    )
}

fn parse_header_line(line: &str) -> (String, String) {
    let v: Vec<&str> = line.splitn(2, ':').collect();
    (
        v[0].trim().to_string(), // name
        v[1].trim().to_string(), // value
    )
}

fn error_404_handler(_: &Request) -> Response {
    let status = Status::NotFound;
    let body = "Not Found\r\n";

    let mut headers = Headers::new();
    headers.insert("Content-Length".to_string(), body.len().to_string());
    headers.insert("Content-Type".to_string(), "text/plain".to_string());

    Response::new(status, headers, body)
}
