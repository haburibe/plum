///
/// * https://avacariu.me/articles/2015/rust-echo-server-example
/// * https://dfockler.github.io/2016/05/20/web-server.html
/// * https://github.com/iron/iron
///
mod http;

fn main() {
    let host = "127.0.0.1";
    let port = 5050;

    let mut app = http::Application::new();
    app.get("/", Box::new(index_handler));

    app.listen(host, port);
}

fn index_handler(request: &http::Request) -> http::Response {
    let status = http::Status::Ok;
    let body = "Hello, world\r\n";

    let mut headers = http::Headers::new();
    headers.insert("Content-Length".to_string(), body.len().to_string());
    headers.insert("Content-Type".to_string(), "text/plain".to_string());

    http::Response::new(status, headers, body)
}
